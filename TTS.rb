#!/usr/bin/env ruby
# The following packages should be installed for the script to be installed:
# rubygem-multipart-post
# rubygem-highline
# rubygem-hpricot
#
# Make sure you edit the @parameters hash before using!
#

require 'rubygems'
require 'date'
require 'highline/import'
require 'hpricot'
require 'net/http'
require 'net/https'
require 'net/http/post/multipart'
require 'uri'

@parameters={
  "time_in"=>"",
  "time_out"=>"",
  "time_break"=>"",
  "weekend_days"=>[],
  "login_url"=>'',
  "timesheet_path"=>'',
}

# You normally don't need to do any change below this line

def create_timesheet_metadata(data)
  editts_csrf_token = (Hpricot(data)/"input"/"#editTimesheet__csrf_token").first[:value]
  editts_tsId = (Hpricot(data)/"input"/"#editTimesheet_txtTimesheetId").first[:value]
  editts_employeeNumber = (Hpricot(data)/"input"/"#editTimesheet_employeeNumber").first[:value]
  editts_yearChoices = (Hpricot(data)/"select"/"#editTimesheet_yearChoices/option[@selected=selected]").first[:value]
  editts_monthChoices = (Hpricot(data)/"select"/"#editTimesheet_monthChoices/option[@selected=selected]").first[:value]

  metadata_hash = Hash.new
  metadata_hash["editTimesheet[_csrf_token]"] = editts_csrf_token
  metadata_hash["editTimesheet[txtTimesheetId]"] = editts_tsId
  metadata_hash["editTimesheet[show]"] = ""
  metadata_hash["editTimesheet[txtAction]"] = "save"
  metadata_hash["editTimesheet[employeeNumber]"] = editts_employeeNumber
  metadata_hash["editTimesheet[yearChoices]"] = editts_yearChoices
  metadata_hash["editTimesheet[monthChoices]"] = editts_monthChoices
  metadata_hash["editTimesheet[submitComment]"] = ""
  metadata_hash
end

def create_repost_data_input_hash(data,field)
  i=0
  last_id = field + i.to_s
  field_hash = Hash.new
  while last_id_elem = (Hpricot(data)/last_id).first
    field_hash[last_id_elem[:name]]= last_id_elem[:value] || ""
    i+=1
    last_id = field + i.to_s
  end
  field_hash
end

def create_repost_data_select_hash(data,field)
  i=0
  last_id = field + i.to_s
  field_hash = Hash.new
  while last_id_elem = (Hpricot(data)/last_id).first
    if selected = (last_id_elem/"option[@selected=selected]").first
      field_hash[last_id_elem[:name]] = selected[:value]
    else
      field_hash[last_id_elem[:name]] = "1"
    end
    i+=1
    last_id = field + i.to_s
  end
  field_hash
end

# Gererates the timesheet data (in_time, out_time, break_duration)
def generate_timesheet_data_hash(year,month)
  ts_hash = Hash.new
  i = 0
  day=Date.new(year,month,1)
  while day.month == month 
    if @parameters["weekend_days"].include? day.wday
      ts_hash["in_time[" + i.to_s + "]"] = ""
      ts_hash["out_time[" + i.to_s + "]"] = ""
      ts_hash["break_duration[" + i.to_s + "]"] = ""
      ts_hash["row_total_duration[" + i.to_s + "]"] = ""
      i += 1
      day += 1
      next
    end
    ts_hash["in_time[" + i.to_s + "]"] = @parameters["time_in"]
    ts_hash["out_time[" + i.to_s + "]"] = @parameters["time_out"]
    ts_hash["break_duration[" + i.to_s + "]"] = @parameters["time_break"]
    i+=1
    day += 1
  end
  ts_hash
end


data_hash = Hash.new
headers = Hash.new
username = ask("Enter your username:  ") { |q| q.echo = true }
password = ask("Enter your password:  " ) { |q| q.echo = "*"}
uri = URI(@parameters['login_url'])

# Start session for all the transactions
Net::HTTP.start(uri.host, uri.port,
  :use_ssl => uri.scheme == 'https') do |http|
  request = Net::HTTP::Get.new uri.request_uri

  # Get the login page and login
  response = http.request request
  cookie = response['set-cookie']
  login_csrf_token = (Hpricot(response.body)/"input"/"#login__csrf_token").first[:value]
  data_hash = Hash.new
  data_hash["login[txtUserName]"] = username
  data_hash["login[txtUserPassword]"] = password
  data_hash["login[_csrf_token]"] = login_csrf_token
  headers = {
    'Cookie' => cookie,
    'Referer' => @parameters['login_url'],
    'Content-Type' => 'multipart/form-data'
  }

  res = http.request(Net::HTTP::Post::Multipart.new('/', data_hash, headers))

  # Get the timesheet page and create post data
  res = http.get(@parameters['timesheet_path'], headers)
  data_hash=data_hash.merge(create_timesheet_metadata(res.body))
  data_hash=data_hash.merge(create_repost_data_input_hash(res.body,"#original_record_ids_"))
  data_hash=data_hash.merge(create_repost_data_input_hash(res.body,"#record_id_"))
  data_hash=data_hash.merge(create_repost_data_input_hash(res.body,"#date_"))
  data_hash=data_hash.merge(create_repost_data_input_hash(res.body,"#medical_checkup_duration_"))
  data_hash=data_hash.merge(create_repost_data_input_hash(res.body,"#comment_"))
  data_hash=data_hash.merge(create_repost_data_select_hash(res.body,"#classification_id_"))
  data_hash=data_hash.merge(generate_timesheet_data_hash(data_hash["editTimesheet[yearChoices]"].to_i,data_hash["editTimesheet[monthChoices]"].to_i))

  # Save the timesheets
  res = http.request(Net::HTTP::Post::Multipart.new((Hpricot(res.body)/"form"/"#editTimesheet").first[:action], data_hash, headers))
end

print "The timesheets should have been saved. Please do a manual check before submitting!\nPlease report bugs, comments and improvements at ctrianta@rh.c.\n"
